﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_AddProduct.aspx.vb" Inherits="NPO_WMS.MS_AddProduct" %>

<%@ Register Src="~/css_srcipt.ascx" TagPrefix="uc1" TagName="css_srcipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <link rel="stylesheet" href="global/vendor/filament-tablesaw/tablesaw.css">
  <link rel="stylesheet" href="global/vendor/ladda-bootstrap/ladda.css">
  <link rel="stylesheet" href="assets/examples/css/uikit/buttons.css">
  <title>Master-Product | WMS</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-calendar-note" aria-hidden="true"></i>
                <span class="site-menu-title">Activity</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การรับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบรับสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">สถานะใบรับสินะค้า</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การเบิกสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสารใบเบิกสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">สถานะใบเบิกสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การโอนสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบโอนสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบโอนสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ใบประกอบสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบประกอบสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบประกอบสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                <span class="site-menu-title">Status</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Forms</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Tables</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Chart</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">Chart.js</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">Gauges</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/flot.html">
                        <span class="site-menu-title">Flot</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-home" aria-hidden="true"></i>
                <span class="site-menu-title">Warehouse</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">สรุปยอดคลังสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ประวัติความเคลื่อนไหว</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-check-circle-u" aria-hidden="true"></i>
                <span class="site-menu-title">Checking</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจนับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบตรวจนับ</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">ประวัติการตรวจนับสินค้า</span>
                      </a>  
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจ QC</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสาร QC</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">ประวัติการตรวจ QC</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub active">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                <span class="site-menu-title">Master</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub active">
                  <a href="MS_ProductGroup.aspx">
                    <span class="site-menu-title">Product (ข้อมูลสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Warehouse.aspx">
                    <span class="site-menu-title">Warehouse (คลังสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Staff.aspx">
                    <span class="site-menu-title">Staff (ข้อมูลพนักงาน)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Supplier.aspx">
                    <span class="site-menu-title">Contact (ข้อมูลผู้ติดต่อ)</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="page animsition">
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner">
        <section class="page-aside-section">
          <h5 class="page-aside-title">Master (ข้อมูลพื้นฐาน)</h5>
          <div class="list-group">
            <a class="list-group-item" href="MS_ProductGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product Group</a>
            <a class="list-group-item" href="MS_ProductType.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product Type</a>
            <a class="list-group-item active" href="MS_Product.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product</a>
            <a class="list-group-item" href="MS_SKUGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU Group</a>
            <a class="list-group-item" href="MS_SKU.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU</a>
            <a class="list-group-item" href="MS_Tag.aspx"><i class="icon md-label" aria-hidden="true"></i>Tag</a>
            <a class="list-group-item" href="MS_UOM.aspx"><i class="icon md-file-text" aria-hidden="true"></i>UOM</a>
            
          </div>
        </section>
   
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h4 class="page-title">Product</h4>
      </div>
      <div class="page-content container-fluid">
      <div class="row">
        <div class="col-lg-12">
         <!-- Panel Kitchen Sink -->
          <div class="panel">
            <header class="panel-heading">
              <h5 class="panel-title">
               
              </h5>
            </header>
            <div class="panel-body">
              <div class="row row-lg">
              <!-- Example Tabs Icon -->
              <div class="example-wrap">
                  <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-2 control-label">Project Group :</label>
                        <div class="form-group  col-sm-4">
                             <select data-plugin="selectpicker" title='Choose'>
                            <optgroup label="Picnic">
                              <option>Mustard</option>
                              <option>Ketchup</option>
                              <option>Relish</option>
                            </optgroup>
                            <optgroup label="Camping">
                              <option>Tent</option>
                              <option disabled>Flashlight</option>
                              <option>Toilet Paper</option>
                            </optgroup>
                          </select>
                          </div>
                      <label class="col-sm-2 control-label">Product Type :</label>
                          <div class="form-group  col-sm-4">
                             <select data-plugin="selectpicker" title='Choose'>
                            <optgroup label="Picnic">
                              <option>Mustard</option>
                              <option>Ketchup</option>
                              <option>Relish</option>
                            </optgroup>
                            <optgroup label="Camping">
                              <option>Tent</option>
                              <option disabled>Flashlight</option>
                              <option>Toilet Paper</option>
                            </optgroup>
                          </select>
                          </div>
                    </div>
                   
               <div class="clearfix"></div>
                <div class="nav-tabs-horizontal">
                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a data-toggle="tab" href="#info" aria-controls="One"
                      role="tab"><i class="icon md-file-text margin-2" aria-hidden="true"></i>Infomation</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#extra" aria-controls="Two"
                      role="tab"><i class="icon md-file-text margin-2" aria-hidden="true"></i>Extra Info</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#vendor" aria-controls="Three"
                      role="tab"><i class="icon md-file-text margin-2" aria-hidden="true"></i>Product Vendors</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#movement" aria-controls="Four"
                      role="tab"><i class="icon md-file-text margin-2" aria-hidden="true"></i>Movement History</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#order" aria-controls="Five"
                      role="tab"><i class="icon md-file-text margin-2" aria-hidden="true"></i>Order History</a></li>
                  </ul>
                  <div class="tab-content padding-top-15">

                        <div class="tab-pane active" id="info" role="tabpanel">
                         <div class="col-sm-12">
                          <h5><b class="text-primary">Infomation (ข้อมูลทั่วไป)</b></h5><br />
                          <label class="col-sm-8 control-label text-right">Picture :</label>
                          <div class="form-group  col-sm-4 pull-right">
                             <img class="img-rounded" width="160" height="160" src="global/photos/placeholder.png" alt="...">
                          </div>
                        </div>
                       <div class="clearfix"></div>
                        <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">ID/Code Product :</label>
                          <div class="form-group  col-sm-4">
                             <input class="form-control" placeholder="id sku">
                          </div>
                           <div class="col-sm-5 text-center">
                                <button type="button" class="btn btn-floating btn-success btn-sm" data-toggle="tooltip" title="Add Picture"><i class="icon md-plus"></i></button>
                                <button type="reset" class="btn btn-floating btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="icon md-delete"></i></button>
                              </div>
                        </div>
                       <div class="clearfix"></div>
                         <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">Product Name (ชื่อไทย) :</label>
                          <div class="form-group  col-sm-9">
                             <input class="form-control" placeholder="Name Thai">
                          </div>
                        </div>
                        <div class="clearfix"></div>
                         <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">Product Name (ชื่ออังกฤษ) :</label>
                          <div class="form-group  col-sm-9">
                             <input class="form-control" placeholder="Name English">
                          </div>
                        </div>
                        <div class="clearfix"></div>
                         <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">Detail (รายละเอียด) :</label>
                          <div class="form-group  col-sm-9">
                             <textarea class="form-control" id="textareaDefault" rows="3" placeholder="Detail"></textarea>
                          </div>
                        </div>
                       <div class="clearfix"></div>
                       <div class="col-sm-12">
                            <hr />
                            <h5><b class="text-primary">Additional Detail (รายละเอียดเพิ่มเติม)</b></h5><br />
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">Barcode1 :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control text-right" placeholder="000001">
                          </div>
                         <label class="col-sm-3 control-label">Barcode2 :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control text-right" placeholder="000002">
                          </div>
                        </div>
                       <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">Production Date (วันผลิต) :</label>
                          <div class="form-group  col-sm-3">
                             <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon md-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" data-plugin="datepicker" data-multidate="true">
                              </div>
                          </div>

                          <label class="col-sm-3 control-label">Expired Date (วันหมดอายุ) :</label>
                          <div class="form-group  col-sm-3">
                             <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon md-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" data-plugin="datepicker" data-multidate="true">
                              </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                              <label class="col-sm-3 control-label">Manufacturer :</label>
                              <div class="form-group  col-sm-4">
                                 <select data-plugin="selectpicker" title='Supplier'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                            </div>
                           <div class="clearfix"></div>
                              <div class="col-sm-12">
                             <!-- Example Size -->
                              <label class="col-sm-2 control-label">Weight :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Volume :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                              <div class="col-sm-12">
                             <!-- Example Size -->
                              <label class="col-sm-2 control-label">Width :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Height :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Length :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Area :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                            </div>
                       </div>
                        <div class="tab-pane" id="extra" role="tabpanel">
                          <div class="col-sm-12">
                            <h5><b class="text-primary">Inventory (คลังสินค้า)</b></h5><br />
                             
                              <label class="col-sm-2 control-label">Warehouse :</label>
                              <div class="form-group  col-sm-6">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>Warehouse</option>
                                  <option>Warehouse</option>
                                  <option>Warehouse</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Floor :</label>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Select'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                            </div>
                           <div class="clearfix"></div>
                             <div class="col-sm-12">
                             <!-- Example Size -->
                              <label class="col-sm-2 control-label">Room :</label>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Select'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Zone :</label>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Select'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Lock :</label>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Select'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                             </div>
                           <div class="clearfix"></div>
                            <div class="col-sm-12">
                              <label class="col-sm-2 control-label">Row :</label>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Select'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Shelf :</label>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Select'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                             <label class="col-sm-2 control-label">Deep :</label>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>Warehouse</option>
                                  <option>Warehouse</option>
                                  <option>Warehouse</option>
                                </select>
                            </div>
                             <div class="col-sm-12">
                              <hr />
                               <h5><b class="text-primary">Unit Product (หน่วยสินค้า)</b></h5><br />
                                  <label class="col-sm-2 control-label">Weight :</label>
                                  <div class="form-group  col-sm-2">
                                     <input class="form-control text-right" placeholder="0.00">
                                  </div>
                                  <div class="form-group  col-sm-2">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>THB</option>
                                      <option>THB</option>
                                      <option>THB</option>
                                  </select>
                                  </div>
                                  <label class="col-sm-2 control-label">Volume :</label>
                                  <div class="form-group  col-sm-2">
                                     <input class="form-control text-right" placeholder="0.00">
                                  </div>
                                  <div class="form-group  col-sm-2">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>THB</option>
                                      <option>THB</option>
                                      <option>THB</option>
                                  </select>
                                  </div>
                                </div>
                            <div class="clearfix"></div>
                              <div class="col-sm-12">
                             <!-- Example Size -->
                              <label class="col-sm-2 control-label">Width :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Height :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Length :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Area :</label>
                              <div class="form-group  col-sm-2">
                                 <input class="form-control text-right" placeholder="0.00">
                              </div>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                            </div>
                     
                           <div class="clearfix"></div>
                             <div class="col-sm-12">
                              <hr />
                               <h5><b class="text-primary">Cost and Price (ค่าใช้จ่ายและราคา)</b></h5><br />
                                  <label class="col-sm-2 control-label">Cost :</label>
                                  <div class="form-group  col-sm-2">
                                     <input class="form-control text-right" placeholder="0.00">
                                  </div>
                                  <div class="form-group  col-sm-2">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>THB</option>
                                      <option>THB</option>
                                      <option>THB</option>
                                  </select>
                                  </div>
                                  <label class="col-sm-2 control-label">Sale Price :</label>
                                  <div class="form-group  col-sm-2">
                                     <input class="form-control text-right" placeholder="0.00">
                                  </div>
                                  <div class="form-group  col-sm-2">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>THB</option>
                                      <option>THB</option>
                                      <option>THB</option>
                                  </select>
                                  </div>
                               </div>
                                <div class="col-sm-12">
                                  <label class="col-sm-2 control-label">Normal Price :</label>
                                  <div class="form-group  col-sm-2">
                                     <input class="form-control text-right" placeholder="0.00">
                                  </div>
                                  <div class="form-group  col-sm-2">
                                    
                                     <input class="form-control text-right" placeholder="0.00">
                                  </div>
                                  <label class="col-sm-1 control-label">% </label>
                               </div>
                             </div>
                           </div>
                        
                         <div class="tab-pane" id="vendor" role="tabpanel">
                            <div class="col-sm-12">
                            <h5><b class="text-primary">Vendor (ผู้ขาย)</b></h5><br />
                             
                              <label class="col-sm-2 control-label">Name :</label>
                              <div class="form-group  col-sm-6">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>Warehouse</option>
                                  <option>Warehouse</option>
                                  <option>Warehouse</option>
                              </select>
                              </div>
                              <label class="col-sm-2 control-label">Price :</label>
                              <div class="form-group  col-sm-2">
                                 <select data-plugin="selectpicker" title='Select'>
                                  <option>THB</option>
                                  <option>THB</option>
                                  <option>THB</option>
                              </select>
                              </div>
                            </div>
                            <div class="col-sm-12">
                              <label class="col-sm-2 control-label">Product Code :</label>
                              <div class="form-group  col-sm-6">
                                 <select data-plugin="selectpicker" title='Unit'>
                                  <option>Warehouse</option>
                                  <option>Warehouse</option>
                                  <option>Warehouse</option>
                              </select>
                              </div>
                            </div>
                            <div class="col-sm-12">
                              <label class="col-sm-3 control-label">Active Status :</label>
                              <div class="form-group  col-sm-9">
                                  <div class="pull-left margin-right-20">
                                <input type="checkbox" id="inputBasicOn" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                               </div>
                                <label class="padding-top-3" for="inputBasicOn">On</label>
                                </div>
                              </div>
                          </div>
                         <div class="tab-pane" id="movement" role="tabpanel">
                          <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                              <tr class="bg-blue-grey-100">
                                <th>transaction Type</th>
                                <th class="text-center">Location</th>
                                <th class="text-center">Order Number</th>
                                <th class="text-center">Qty Before</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Qty Affter</th>
                            </thead>
                            <tbody>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                            </tbody>
                          </table>
                         </div>
                         <div class="tab-pane" id="order" role="tabpanel">
                            <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                              <tr class="bg-blue-grey-100">
                                <th>Type</th>
                                <th class="text-center">Order</th>
                                <th class="text-center">Custermer/Vendor</th>
                                <th class="text-center">Date</th>
                                <th class="text-center">Order Status</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Sub-Total</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                              <tr>
                                <td>x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                                <td class="text-center">x</td>
                              </tr>
                            </tbody>
                          </table>
                         </div>
                        <hr />
                    <%-- <div class="form-group form-material">
                      <div class="col-sm-12 col-sm-offset-9">
                        <button type="button" class="btn btn-primary">Submit </button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                      </div>
                     </div>--%>
                    </div>
                  </div>
                  </div>
                </div> <!-- End Example Tabs Icon -->
               </div>
               </div>
           <!-- End Panel Kitchen Sink -->
           </div>
         </div> 
        </div>
      </div>
    </div>
    <uc1:css_srcipt runat="server" ID="css_srcipt" />
</asp:Content>

