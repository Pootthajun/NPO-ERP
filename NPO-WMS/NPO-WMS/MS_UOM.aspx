﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_UOM.aspx.vb" Inherits="NPO_WMS.MS_UOM" %>

<%@ Register Src="~/css_srcipt.ascx" TagPrefix="uc1" TagName="css_srcipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <link rel="stylesheet" href="assets/examples/css/uikit/modals.css">
  <link rel="stylesheet" href="global/vendor/filament-tablesaw/tablesaw.css">
 
  <title>Master-UOM | WMS</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-calendar-note" aria-hidden="true"></i>
                <span class="site-menu-title">Activity</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การรับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบรับสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">สถานะใบรับสินะค้า</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การเบิกสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสารใบเบิกสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">สถานะใบเบิกสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การโอนสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบโอนสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบโอนสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ใบประกอบสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบประกอบสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบประกอบสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                <span class="site-menu-title">Status</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Forms</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Tables</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Chart</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">Chart.js</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">Gauges</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/flot.html">
                        <span class="site-menu-title">Flot</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-home" aria-hidden="true"></i>
                <span class="site-menu-title">Warehouse</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">สรุปยอดคลังสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ประวัติความเคลื่อนไหว</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-check-circle-u" aria-hidden="true"></i>
                <span class="site-menu-title">Checking</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจนับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบตรวจนับ</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">ประวัติการตรวจนับสินค้า</span>
                      </a>  
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจ QC</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสาร QC</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">ประวัติการตรวจ QC</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub active">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                <span class="site-menu-title">Master</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub active">
                  <a href="MS_ProductGroup.aspx">
                    <span class="site-menu-title">Product (ข้อมูลสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Warehouse.aspx">
                    <span class="site-menu-title">Warehouse (คลังสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Staff.aspx">
                    <span class="site-menu-title">Staff (ข้อมูลพนักงาน)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Supplier.aspx">
                    <span class="site-menu-title">Contact (ข้อมูลผู้ติดต่อ)</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="page animsition">
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner">
        <section class="page-aside-section">
          <h5 class="page-aside-title">Master (ข้อมูลพื้นฐาน)</h5>
          <div class="list-group">
            <a class="list-group-item " href="MS_ProductGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product Group</a>
            <a class="list-group-item " href="MS_ProductType.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product Type</a>
            <a class="list-group-item" href="MS_Product.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product</a>
            <a class="list-group-item" href="MS_SKUGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU Group</a>
            <a class="list-group-item" href="MS_SKU.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU</a>
            <a class="list-group-item" href="MS_Tag.aspx"><i class="icon md-label" aria-hidden="true"></i>Tag</a>
            <a class="list-group-item active" href="MS_UOM.aspx"><i class="icon md-file-text" aria-hidden="true"></i>UOM</a>
            
          </div>
        </section>
   
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h4 class="page-title">Unit Of Measure (UOM)</h4>
      </div>
      <div class="page-content container-fluid">
      <div class="row">
        <div class="col-lg-12">
         <!-- Panel Kitchen Sink -->
          <div class="panel">
            <header class="panel-heading">
              <h5 class="panel-title">
               
              </h5>
            </header>
            <div class="panel-body">
              <div class="col-sm-12">
                <div class="nav-tabs-horizontal nav-tabs-inverse">
                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li class="active" role="presentation">
                      <a data-toggle="tab" href="#Currency" aria-controls="exampleTabsLeftInverseOne" role="tab">Currency</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Volume" aria-controls="exampleTabsLeftInverseTwo" role="tab">Volume</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Weight" aria-controls="exampleTabsLeftInverseThree" role="tab">Weight</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Length" aria-controls="exampleTabsLeftInverseTwo" role="tab">Length</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Count" aria-controls="exampleTabsLeftInverseThree" role="tab">Count</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Area" aria-controls="exampleTabsLeftInverseThree" role="tab">Area </a>
                    </li>
                  </ul>
                  <div class="tab-content padding-20">
                    <div class="tab-pane active" id="Currency" role="tabpanel">
                      <button type="button" class="btn btn-warning" data-target="#AddCurrency" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PCurrency"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Name</th>
                            <th class="text-center">Short Name</th>
                            <th class="text-center">Sign</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Thai Bath</td>
                            <td class="text-center">THB</td>
                            <td class="text-center">฿</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>US Dollar</td>
                            <td class="text-center">USD</td>
                            <td class="text-center">$</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Japanese Yen</td>
                            <td class="text-center">JPY</td>
                            <td class="text-center">¥</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Pound Sterling</td>
                            <td class="text-center">GBP</td>
                            <td class="text-center">₤</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Russian Ruble</td>
                            <td class="text-center">RUB</td>
                            <td class="text-center">₱</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>UA Hryvna</td>
                            <td class="text-center">UAH</td>
                            <td class="text-center">₴</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>EURO</td>
                            <td class="text-center">EUR</td>
                            <td class="text-center">€</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="tab-pane" id="Volume" role="tabpanel">
                        <button type="button" class="btn btn-warning" data-target="#AddOther" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PVolume"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Name</th>
                            <th class="text-center">Short Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Cubic meter</td>
                            <td class="text-center">m3</td>
                            <td class="text-center"><a class="icon md-edit"></a></td>
                            <td class="text-center"><a class="icon md-delete"></a></td>
                          </tr>
                          <tr>
                            <td>Barrel</td>
                            <td class="text-center">bbl</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Cubic centimeter</td>
                            <td class="text-center">cu.cm</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Cubic inch</td>
                            <td class="text-center">cu.in</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Gallon</td>
                            <td class="text-center">gal</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Milliliter</td>
                            <td class="text-center">ml</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Liter</td>
                            <td class="text-center">l</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="tab-pane" id="Weight" role="tabpanel">
                       <button type="button" class="btn btn-warning" data-target="#AddOther" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PWeight"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Name</th>
                            <th class="text-center">Short Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Ounce</td>
                            <td class="text-center">oz</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Pound</td>
                            <td class="text-center">lb</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Gram</td>
                            <td class="text-center">gr</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Metric ton</td>
                            <td class="text-center">tn</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Kilogram</td>
                            <td class="text-center">kg</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="tab-pane" id="Length" role="tabpanel">
                        <button type="button" class="btn btn-warning" data-target="#AddOther" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PLength"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                     <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Name</th>
                            <th class="text-center">Short Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Kilometer</td>
                            <td class="text-center">km</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Millimeter</td>
                            <td class="text-center">mm</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Centimeter</td>
                            <td class="text-center">cm</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Meter</td>
                            <td class="text-center">m</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Foot</td>
                            <td class="text-center">ft</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Inch</td>
                            <td class="text-center">in</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Yard</td>
                            <td class="text-center">yd</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="tab-pane" id="Count" role="tabpanel">
                        <button type="button" class="btn btn-warning" data-target="#AddOther" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PCount"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                     <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Name</th>
                            <th class="text-center">Short Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Dozen</td>
                            <td class="text-center">dz</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Pair</td>
                            <td class="text-center">pr</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Each</td>
                            <td class="text-center">ea</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                      
                    <div class="tab-pane" id="Area" role="tabpanel">
                      <button type="button" class="btn btn-warning" data-target="#AddOther" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PArea"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Name</th>
                            <th class="text-center">Short Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Square inch</td>
                            <td class="text-center">sq.in</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Square meter</td>
                            <td class="text-center">sq.m</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Square foot</td>
                            <td class="text-center">sq.ft</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>

   <!-------------------------- Modal AddCurrency--------------------------------->
                        <div class="modal fade" id="AddCurrency" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add Currency</h4>
                              </div><hr />
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Name :</label>
                                   <div class="form-group  col-sm-9">
                                     <input class="form-control" placeholder="Name">
                                     </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Short Name :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control" placeholder="Short Name">
                                     </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Sign :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control" placeholder="Sign">
                                     </div>
                                  </div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-4 control-label">Active Status :</label>
                                  <div class="form-group  col-sm-4">
                                      <div class="pull-left margin-right-20">
                                    <input type="checkbox" id="inputBasicOn" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                  </div>
                                  <label class="padding-top-3" for="inputBasicOn">On</label>
                                  </div>
                                </div>
                                </div>
                              </form>
                            </div><hr />
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-primary">Submit </button>
                                 <button type="reset" class="btn btn-warning">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>

   <!-------------------------- Modal AddOther--------------------------------->
                        <div class="modal fade" id="AddOther" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add UOM</h4>
                              </div><hr />
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Name :</label>
                                   <div class="form-group  col-sm-9">
                                     <input class="form-control" placeholder="Name">
                                     </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Short Name :</label>
                                   <div class="form-group  col-sm-5">
                                     <input class="form-control" placeholder="Short Name">
                                     </div>
                                  </div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-4 control-label">Active Status :</label>
                                  <div class="form-group  col-sm-4">
                                      <div class="pull-left margin-right-20">
                                    <input type="checkbox" id="inputOn" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                  </div>
                                  <label class="padding-top-3" for="inputBasicOn">On</label>
                                  </div>
                                </div>
                                </div>
                              </form>
                            </div><hr />
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-primary">Submit </button>
                                 <button type="reset" class="btn btn-warning">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>
                  </div>
                 </div>
                </div> 
              </div>
           <!-- End Panel Kitchen Sink -->
           </div>
         </div> 
        </div>
      </div>
    </div>
  </div>
    <uc1:css_srcipt runat="server" ID="css_srcipt" />
</asp:Content>