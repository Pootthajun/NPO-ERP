﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_Pallet.aspx.vb" Inherits="NPO_WMS.MS_Pallet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <link rel="stylesheet" href="assets/examples/css/uikit/modals.css">
  <link rel="stylesheet" href="global/vendor/filament-tablesaw/tablesaw.css">
<!-- Plugins -->
  <link rel="stylesheet" href="global/vendor/select2/select2.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="global/vendor/asrange/asRange.css">
  <link rel="stylesheet" href="global/vendor/asspinner/asSpinner.css">
  <link rel="stylesheet" href="global/vendor/clockpicker/clockpicker.css">
  <link rel="stylesheet" href="global/vendor/ascolorpicker/asColorPicker.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-touchspin/bootstrap-touchspin.css">
  <link rel="stylesheet" href="global/vendor/card/card.css">
  <link rel="stylesheet" href="global/vendor/jquery-labelauty/jquery-labelauty.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
  <link rel="stylesheet" href="global/vendor/jt-timepicker/jquery-timepicker.css">
  <link rel="stylesheet" href="global/vendor/jquery-strength/jquery-strength.css">
  <link rel="stylesheet" href="global/vendor/multi-select/multi-select.css">
  <link rel="stylesheet" href="global/vendor/typeahead-js/typeahead.css">
  <link rel="stylesheet" href="assets/examples/css/forms/advanced.css">
  
  <title>Master-Pallet | WMS</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-calendar-note" aria-hidden="true"></i>
                <span class="site-menu-title">Activity</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การรับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบรับสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">สถานะใบรับสินะค้า</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การเบิกสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสารใบเบิกสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">สถานะใบเบิกสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การโอนสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบโอนสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบโอนสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ใบประกอบสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบประกอบสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบประกอบสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                <span class="site-menu-title">Status</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Forms</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Tables</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Chart</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">Chart.js</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">Gauges</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/flot.html">
                        <span class="site-menu-title">Flot</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-home" aria-hidden="true"></i>
                <span class="site-menu-title">Warehouse</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">สรุปยอดคลังสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ประวัติความเคลื่อนไหว</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-check-circle-u" aria-hidden="true"></i>
                <span class="site-menu-title">Checking</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจนับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบตรวจนับ</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">ประวัติการตรวจนับสินค้า</span>
                      </a>  
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจ QC</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสาร QC</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">ประวัติการตรวจ QC</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub active">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                <span class="site-menu-title">Master</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub">
                  <a href="MS_ProductGroup.aspx">
                    <span class="site-menu-title">Product (ข้อมูลสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub active">
                  <a href="MS_Warehouse.aspx">
                    <span class="site-menu-title">Warehouse (คลังสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Staff.aspx">
                    <span class="site-menu-title">Staff (ข้อมูลพนักงาน)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Supplier.aspx">
                    <span class="site-menu-title">Contact (ข้อมูลผู้ติดต่อ)</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="page animsition">
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner">
        <section class="page-aside-section">
          <h5 class="page-aside-title">Master (ข้อมูลพื้นฐาน)</h5>
          <div class="list-group">
            <a class="list-group-item" href="MS_Warehouse.aspx"><i class="icon md-home" aria-hidden="true"></i>Warehouse</a>
            <a class="list-group-item" href="MS_Locations.aspx"><i class="icon md-pin" aria-hidden="true"></i>Location</a>
            <a class="list-group-item" href="MS_TypePallet.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Type Pallet</a>
            <a class="list-group-item active" href="MS_Pallet.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Pallet</a>
            
          </div>
        </section>
        
        <style>
            table {
                    width: 100%;
                  }
                 .row-ID {
                  width: 15%;
                }
                .row-Type {
                  width: 65%;
                }
                .row-Edit {
                  width: 10%;
                }
                .row-Delete {
                  width: 10%;
                }
           
            td.text-center{
                   text-align: center;
                 }
        </style>
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h4 class="page-title">Pallet</h4>
      </div>
      <div class="page-content container-fluid">
      <div class="row">
        <div class="col-lg-12">
         <!-- Panel Kitchen Sink -->
          <div class="panel">
            <header class="panel-heading">
              <h5 class="panel-title">
               
              </h5>
            </header>
            <div class="panel-body">
                    <button type="button" class="btn btn-warning" data-target="#AddPallet" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                    <div class="btn-group pull-right" role="group">
                      <button type="button" class="btn btn-info dropdown-toggle" id="exampleGroupDrop1"
                      data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                        Print
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                      </ul>
                    </div>
                 <div class="clearfix visible-md-block"></div><br /><br />
                  <table class="tablesaw table-striped table-bordered table-hover">
                    <thead>
                      <tr class="bg-blue-grey-100">
                        <th class="row-1 row-ID">ID </th>
                        <th class="row-2 row-Product">Pallet Type</th>
                        <th class="row-2 row-Product">Description</th>
                        <th class="text-center row-3 row-Edit" >Edit</th>
                        <th class="text-center row-4 row-Delete">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                     <tr>
                        <td>1</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                      <tr>
                        <td>7</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                      <tr>
                        <td>8</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                      <tr>
                        <td>9</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                      <tr>
                        <td>10</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><a class="icon md-edit"></a></td>
                        <td class="text-center"><a class="icon md-delete"></a></td>
                      </tr>
                 
                    </tbody>
                  </table>

            <!-------------------------- Modal AddPallet--------------------------------->
                        <div class="modal fade" id="AddPallet" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add Pallet</h4>
                              </div>
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Type Pallet :</label>
                                  <div class="form-group  col-sm-5">
                                     <select data-plugin="selectpicker" data-live-search="true">
                                        <option>Pallet 1</option>
                                        <option>Pallet 2</option>
                                        <option>Pallet 3</option>
                                      </select>
                                  </div>
                                </div>
                               <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">ID Pallet :</label>
                                  <div class="form-group  col-sm-5">
                                    <input class="form-control" placeholder="Pallet">
                                  </div>
                                </div>
                               <div class="clearfix"></div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Description :</label>
                                  <div class="form-group  col-sm-9">
                                      <textarea class="form-control" id="textDescription" rows="3"></textarea>
                                  </div>
                                </div>
               
                               <div class="clearfix"></div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Active Status :</label>
                                  <div class="form-group  col-sm-9">
                                      <div class="pull-left margin-right-20">
                                    <input type="checkbox" id="inputBasicOn" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                  </div>
                                  <label class="padding-top-3" for="inputBasicOn">On</label>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                               <div class="modal-footer">
                                     <button type="button" class="btn btn-primary">Submit </button>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                </div>          
                             </div>
                          </form>
                       </div>
                    </div>
                </div>
              </div>
            <!-- End Panel Kitchen Sink -->
          </div>
        </div>
      </div>
    </div>
      </div> <!-- Core  -->
  </div>
  </div>
  <script src="global/vendor/jquery/jquery.js"></script>
  <script src="global/vendor/bootstrap/bootstrap.js"></script>
  <script src="global/vendor/animsition/animsition.js"></script>
  <script src="global/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="global/vendor/waves/waves.js"></script>
 
  <!-- Plugins -->
  <script src="global/vendor/switchery/switchery.min.js"></script>
  <script src="global/vendor/intro-js/intro.js"></script>
  <script src="global/vendor/screenfull/screenfull.js"></script>
  <script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="global/vendor/select2/select2.min.js"></script>
  <script src="global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="global/vendor/switchery/switchery.min.js"></script>
  <script src="global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="global/vendor/asspinner/jquery-asSpinner.min.js"></script>
  <script src="global/vendor/ascolor/jquery-asColor.min.js"></script>
 <!-- Scripts -->
  <script src="global/js/core.js"></script>
  <script src="assets/js/site.js"></script>
  <script src="assets/js/sections/menu.js"></script>
  <script src="assets/js/sections/menubar.js"></script>
  <script src="assets/js/sections/sidebar.js"></script>
  <script src="global/js/configs/config-colors.js"></script>
  <script src="global/js/components/asscrollable.js"></script>
  <script src="global/js/components/animsition.js"></script>
  <script src="global/js/components/slidepanel.js"></script>
  <script src="global/js/components/switchery.js"></script>
  <script src="global/js/components/select2.js"></script>
  <script src="global/js/components/bootstrap-select.js"></script>
  <script src="global/js/components/icheck.js"></script>
  <script src="global/js/components/switchery.js"></script>
  <script src="global/js/components/multi-select.js"></script>
  <script src="assets/examples/js/forms/advanced.js"></script>
  <script src="global/js/components/material.js"></script>

  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</asp:Content>
