﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="test.aspx.vb" Inherits="NPO_WMS.test" %>

<%@ Register Src="~/css_srcipt.ascx" TagPrefix="uc1" TagName="css_srcipt" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">

    <div class="page animsition">
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner">
        <section class="page-aside-section">
          <h5 class="page-aside-title">Master (ข้อมูลพื้นฐาน)</h5>
          <div class="list-group">
            <a class="list-group-item " href="MS_ProductGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product Group</a>
            <a class="list-group-item active" href="MS_Product.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product</a>
            <a class="list-group-item" href="MS_SKUGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU Group</a>
            <a class="list-group-item" href="MS_SKU.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU</a>
            <a class="list-group-item" href="MS_Locations.aspx"><i class="icon md-pin" aria-hidden="true"></i>Location</a>
            <a class="list-group-item" href="MS_Warehouse.aspx"><i class="icon md-home" aria-hidden="true"></i>Warehouse</a>
            <a class="list-group-item" href="MS_Pallet.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Pallet</a>
            <a class="list-group-item" href="frmMS_Checker.aspx"><i class="icon md-assignment-check" aria-hidden="true"></i>Checker</a>
            <a class="list-group-item" href="MS_Tag.aspx"><i class="icon md-label" aria-hidden="true"></i>Tag</a>
            <a class="list-group-item" href="MS_UOM.aspx"><i class="icon md-file-text" aria-hidden="true"></i>UOM</a>
            
          </div>
        </section>
   
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h4 class="page-title">Product</h4>
      </div>
      <div class="page-content container-fluid">
      <div class="row">
        <div class="col-lg-12">
         <!-- Panel Kitchen Sink -->
          <div class="panel">
            <header class="panel-heading">
              <h5 class="panel-title">
               
              </h5>
            </header>
            <div class="panel-body">
              <div class="row row-lg">
              <!-- Example Tabs Icon -->
              <div class="example-wrap">
                  <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-3 control-label">ID SKU :</label>
                      <div class="form-group  col-sm-4">
                         <input class="form-control" placeholder="id sku">
                      </div>
                    </div>
                   <div class="clearfix"></div>
                     <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-3 control-label">SKU Name (ชื่อไทย) :</label>
                      <div class="form-group  col-sm-9">
                         <input class="form-control" placeholder="Name Thai">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                     <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-3 control-label">SKU Name (ชื่ออังกฤษ) :</label>
                      <div class="form-group  col-sm-9">
                         <input class="form-control" placeholder="Name English">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                <div class="nav-tabs-horizontal">
                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a data-toggle="tab" href="#info" aria-controls="exampleTabsIconOne"
                      role="tab"><i class="icon md-file-text margin-2" aria-hidden="true"></i>Infomation</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#specs" aria-controls="exampleTabsIconTwo"
                      role="tab"><i class="icon md-file-text margin-2" aria-hidden="true"></i>Specs</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#other" aria-controls="exampleTabsIconThree"
                      role="tab"><i class="icon md-file-text margin-2" aria-hidden="true"></i>Other</a></li>
                  </ul>
                  <div class="tab-content padding-top-15">

                    <div class="tab-pane active" id="info" role="tabpanel">
                          <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-8 control-label text-right">Picture :</label>
                      <div class="form-group  col-sm-4 pull-right">
                         <img class="img-rounded" width="160" height="160" src="global/photos/placeholder.png" alt="...">
                      </div>
                    </div>
                   <div class="clearfix"></div>
                      <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-3 control-label">ID SKU :</label>
                      <div class="form-group  col-sm-4">
                         <input class="form-control" placeholder="id sku">
                      </div>
                       <div class="col-sm-5 text-center">
                            <button type="button" class="btn btn-floating btn-success btn-sm" data-toggle="tooltip" title="edit"><i class="icon md-edit"></i></button>
                            <button type="reset" class="btn btn-floating btn-danger btn-sm" data-toggle="tooltip" title="delete"><i class="icon md-delete"></i></button>
                          </div>
                    </div>
                    </div>
                   <div class="clearfix"></div>
                     <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-3 control-label">SKU Name (ชื่อไทย) :</label>
                      <div class="form-group  col-sm-9">
                         <input class="form-control" placeholder="Name Thai">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                     <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-3 control-label">SKU Name (ชื่ออังกฤษ) :</label>
                      <div class="form-group  col-sm-9">
                         <input class="form-control" placeholder="Name English">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                     <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-3 control-label">Detail (รายละเอียด) :</label>
                      <div class="form-group  col-sm-9">
                         <textarea class="form-control" id="textareaDefault" rows="3" placeholder="Detail"></textarea>
                      </div>
                    </div>
                   <div class="clearfix"></div>
                    <div class="col-sm-12">
                       <label class="col-sm-3 control-label">Unit (หน่วย) :</label>
                      <div class="form-group  col-sm-4">
                         <select data-plugin="selectpicker" title='Choose'>
                        <optgroup label="ระบบเมตริก">
                          <option>ลูกบาศก์เซนติเมตร</option>
                          <option>ลูกบาศก์เมตร</option>
                          <option>ลิตร</option>
                        </optgroup>
                        <optgroup label="ระบบอังกฤษ">
                          <option>ช้อนชา </option>
                          <option>ช้อนโต๊ะ</option>
                          <option>ถ้วยตวง  </option>
                        </optgroup>
                        <optgroup label="ระบบอังกฤษ">
                          <option>ช้อนชา </option>
                          <option>ช้อนโต๊ะ</option>
                          <option>ถ้วยตวง  </option>
                          <option>กรัม </option>
                          <option>กิโลกรัม</option>
                          <option>เมตริกตัน ( ตัน ) </option>
                        </optgroup>
                      </select>
                      </div>
                     
                   <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <label class="col-sm-3 control-label">ปริมาตร/หน่วย (ลบ.ม.) :</label>
                        <div class="form-group  col-sm-9">
                        <table class="table table-bordered">
                          <thead>
                          <tr>
                            <th>ปริมาตรกว้าง (ซม.)</th>
                            <th>ปริมาตรยาว (ซม.)</th>
                            <th>ปริมาตรสูง (ซม.)</th>
                            <th class="text-nowrap">รวม (ลบ.ม.)</th>
                          </tr>
                        </thead>
                            <tbody>
                              <tr>
                                <td><input class="form-control" placeholder="0.00"></td>
                                <td><input class="form-control" placeholder="0.00"></td>
                                <td><input class="form-control" placeholder="0.00"></td>
                                <td><input class="form-control" placeholder="0.00" disabled></td>
                              </tr>
                            </tbody>
                         </table>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                     <div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-3 control-label">Production Date (วันผลิต) :</label>
                      <div class="form-group  col-sm-3">
                         <div class="input-group">
                            <span class="input-group-addon">
                              <i class="icon md-calendar" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" data-plugin="datepicker" data-multidate="true">
                          </div>
                      </div>

                      <label class="col-sm-3 control-label">Expired Date (วันหมดอายุ) :</label>
                      <div class="form-group  col-sm-3">
                         <div class="input-group">
                            <span class="input-group-addon">
                              <i class="icon md-calendar" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" data-plugin="datepicker" data-multidate="true">
                          </div>
                      </div>
                    </div>
                    <%--<div class="col-sm-12">
                     <!-- Example Size -->
                      <label class="col-sm-3 control-label">Active Status :</label>
                      <div class="form-group  col-sm-9">
                          <input type="checkbox" data-plugin="switchery" data-color="#2CBA44" checked  data-toggle="tooltip" title="เปิด"/>
                      </div>
                    </div>--%>
                
                    </div>
                   <div class="tab-pane" id="specs" role="tabpanel">
                        
                      <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">ราคาซื้อ/ราคาทุน :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                          <div class="form-group  col-sm-3">
                             <select data-plugin="selectpicker" title='Currency'>
                              <option>THB</option>
                              <option>THB</option>
                              <option>THB</option>
                          </select>
                          </div>
                        </div>
                       <div class="clearfix"></div>
                          <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">ราคาขาย :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                          <div class="form-group  col-sm-3">
                             <select data-plugin="selectpicker" title='Currency'>
                              <option>THB</option>
                              <option>THB</option>
                              <option>THB</option>
                          </select>
                          </div>
                        </div>
                       <div class="clearfix"></div>
                        <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">ราคาอื่นๆ :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                          <div class="form-group  col-sm-3">
                             <select data-plugin="selectpicker" title='Currency'>
                              <option>THB</option>
                              <option>THB</option>
                              <option>THB</option>
                          </select>
                          </div>
                        </div>
                       <div class="clearfix"></div>
                          <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">จำนวนต่ำสุด :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                          <label class="col-sm-3 control-label">จำนวนสูงสุด :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                        </div>
                       <div class="clearfix"></div>
                          <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">น้ำหนักต่ำสุด :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                          <label class="col-sm-3 control-label">น้ำหนักสูงสุด :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                        </div>
                       <div class="clearfix"></div>
                        <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">ปริมาตรต่ำสุด :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                          <label class="col-sm-3 control-label">ปริมาตรสูงสุด :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                        </div>
                       <div class="clearfix"></div>
                       <div class="col-sm-12">
                         <!-- Example Size -->
                          <label class="col-sm-3 control-label">ประเภทพาเลท :</label>
                          <div class="form-group  col-sm-3">
                             <select data-plugin="selectpicker" title='Currency'>
                              <option>THB</option>
                              <option>THB</option>
                              <option>THB</option>
                          </select>
                          </div>
                          <label class="col-sm-3 control-label">จำนวนพาเลท :</label>
                          <div class="form-group  col-sm-3">
                             <input class="form-control" placeholder="0.00">
                          </div>
                        </div>
                       <div class="clearfix"></div>
                        </div>

                    <div class="tab-pane" id="other" role="tabpanel">
                      Quarum eloquentiam, aperta. Hominibus adipiscuntur. Firme graecis doloris liberabuntur
                      sensum recteque declarant. Aiunt, fore tranquillae dicitis
                      necessariae, chorusque periculis libenter constituamus aspernandum
                      ait chaere, cogitemus, quisquis omnia genuit has hae.
                    </div>
                     <div class="form-group form-material">
                      <div class="col-sm-12 col-sm-offset-9">
                        <button type="button" class="btn btn-primary">Submit </button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                      </div>
                     </div>
                    </div>
                  </div>
                  </div>
                </div> <!-- End Example Tabs Icon -->
       
               </div>
             </div>
           <!-- End Panel Kitchen Sink -->
           </div>
         </div> 
        </div>
      </div>
   </div>
    <uc1:css_srcipt runat="server" ID="css_srcipt" />
</asp:Content>
